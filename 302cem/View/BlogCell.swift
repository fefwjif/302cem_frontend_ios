//
//  BlogCell.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class BlogCell: UITableViewCell {
    
    
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var blogAuthor: UILabel!
    @IBOutlet weak var blogTitle: UILabel!
    @IBOutlet weak var blogDate: UILabel!
    @IBOutlet weak var blogContent: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
