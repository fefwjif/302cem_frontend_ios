//
//  BlogCell2.swift
//  302cem
//
//  Created by A on 20/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class BlogCell2: UITableViewCell {
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var postContent: UITextView!
    @IBOutlet weak var postAuthor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
