//
//  MessageVC.swift
//  302cem
//
//  Created by A on 21/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class MessageVC: VC,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var recipientTF: UITextField!
    @IBOutlet weak var messageTV: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var v: UIView!
    
    var message = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.recipientTF.layer.cornerRadius = 10
        self.messageTV.layer.cornerRadius = 10
        self.sendBtn.layer.cornerRadius = 10
        self.tableView.layer.cornerRadius = 10
        self.v.layer.cornerRadius = 10
        
        self.getMessage()
    }
    
    @IBAction func sendBtn(_ sender: Any) {
        if self.recipientTF.text != ""{
            let b = "message=\(self.messageTV.text!)&recipient=\(recipientTF.text!)&sender=\(self.cu.name)"
            self.postMothod(url: self.cf.sendMessage, body: b) { (d) in
                do{
                    self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                    
                    if self.okStruct!.ok == "successful" {
                        
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Done", message: "OK")
                        }
                    } else{
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Error", message: self.okStruct!.ok)
                        }
                    }
                }catch{
                    
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.message.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        
        cell.message.text = self.message[indexPath.row].message
        
        cell.sender.text = "\(self.message[indexPath.row].sender) to me"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 135
       }
    
    @IBAction func reload(_ sender: Any) {
        self.getMessage()
    }
}

extension MessageVC {
    func getMessage(){
       
        let b = "uuid=\(self.cu.uuid)"
        self.postMothod(url: cf.message, body: b) { (d) in
            do{
                self.message = try JSONDecoder().decode([Message].self, from: d)
                self.printLog(msg: self.message.count)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }catch{
           }
        }
    }
}
