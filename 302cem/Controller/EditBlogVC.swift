//
//  EditBlogVC.swift
//  302cem
//
//  Created by A on 20/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class EditBlogVC: VC, UITextFieldDelegate{
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var blogTitle: UITextField!
    @IBOutlet weak var blogContent: UITextView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var v: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.image = self.cu.image
        blogTitle.text = self.cu.title
        blogContent.text = self.cu.content
        
        self.printLog(msg: cu.title)
        
        blogContent.backgroundColor = UIColor.white
        blogContent.textColor = UIColor.black
        blogContent.font = UIFont(name: "Helvetica-Light", size: 20)
        blogContent.textAlignment = .left
        blogContent.keyboardType = .default
        blogContent.returnKeyType = .default
        
        self.image.layer.cornerRadius = 10
        self.imageBtn.layer.cornerRadius = 10
        self.blogTitle.layer.cornerRadius = 10
        self.blogContent.layer.cornerRadius = 10
        self.editBtn.layer.cornerRadius = 10
        self.v.layer.cornerRadius = 10
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func hideKeyboard(tapG:UITapGestureRecognizer){
        self.blogContent.resignFirstResponder()
    }
    
    @IBAction func edit(_ sender: Any) {
        
        let b = "oname=\(self.cu.name)&otitle=\(self.cu.title)&ocontent=\(self.cu.content)&ophoto=\(self.cu.photo)&odate=\(self.cu.date)&name=\(self.cu.name)&title=\(self.blogTitle.text!)&content=\(self.blogContent.text!)&photo=\(self.cu.photo)&date=\(self.cu.date)&uuid=\(self.cu.uuid)"
        
        printLog(msg: b)
        
        self.postMothod(url: self.cf.editBlog, body: b) { (d) in
            do{
                self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                self.printLog(msg: "--------------------self.okStruct!.ok \(self.okStruct!.ok)")
                if self.okStruct!.ok == "successful" {
                    DispatchQueue.main.async {
                        self.displayAlert(title: "Done", message: "OK")
                    }
                } else{
                    DispatchQueue.main.async {
                        self.displayAlert(title: "Error", message: self.okStruct!.ok)
                    }
                }
            }catch{
                
            }
        }
        
        
        
        
    }
}
