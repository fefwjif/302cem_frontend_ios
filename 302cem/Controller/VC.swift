//
//  VC.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit
import Firebase

class VC: UIViewController, PostRequestProtocol,  DisplayEffectProtocol{
    
    var okStruct:OkStruct?
    var cf = config()
    var cu = CurrentUser.sharedInstance
    
    var auth:Auth?
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func displayAlert(title:String, message:String){
        
     let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style:.default , handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController,animated:true, completion: nil)
        
    }
    
    func postMothod(url:String, body :String, myCallBack:@escaping (_ data: Data)  -> ()){
           if let u = URL(string:url){
                     self.printLog(msg: url + body)
                     var urlRequest = URLRequest(url: u)
                     urlRequest.httpMethod = "POST"
                     
                     self.printLog(msg: "\(u)\(body)")
                     if let d = body.data(using: .utf8){
                         let dataTask = URLSession.shared.uploadTask(with: urlRequest, from: d, completionHandler: {
                             data,response,error in
                             
                             if error != nil{
                              self.printLog(msg: error?.localizedDescription)
                             }else{
                              myCallBack(data!)
                             }
                         })
                         dataTask.resume()
                     }
                 }
      }
    
    func printLog(msg: Any) {
    print("-------------------------Log--------------------------")
                print("\(msg)")
                print("")
                print("")
                print("")
     }
     
}
