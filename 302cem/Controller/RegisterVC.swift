//
//  RegisterVC.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class RegisterVC: VC, UITextFieldDelegate {
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userPassword2: UITextField!
    @IBOutlet weak var v: UIView!
    @IBOutlet weak var registerBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.sendSubviewToBack(v)
        self.v.layer.cornerRadius = 10
        self.userName.layer.cornerRadius = 10
        self.userPassword.layer.cornerRadius = 10
        self.userPassword2.layer.cornerRadius = 10
        self.registerBtn.layer.cornerRadius = 10
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification){if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
        var width = self.view.frame.size.width;
        var height = self.view.frame.size.height;
        let rect = CGRect(origin: CGPoint(x: 0,y :-156), size: CGSize(width: width, height: height))
        self.view.frame = rect
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        var width = self.view.frame.size.width;
        var height = self.view.frame.size.height;
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: width, height: height))
        
        self.view.frame = rect
    }
    
    @IBAction func register(_ sender: Any) {
        if(userName.text != "" && userPassword.text != "" && userPassword2.text != "") {
            
            let uniqueString = NSUUID().uuidString
            
            self.printLog(msg: cf.register)
            let b = "name=\(userName.text!)&password=\(userPassword2.text!)&uuid=\(uniqueString)&email=\(userPassword.text!)"
            
            postMothod(url: cf.register, body: b) { (d) in
                
                do{
                    self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                    
                    if self.okStruct!.ok == "successful" {
                        
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Done", message: "OK")
                        }
                    } else{
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Error", message: self.okStruct!.ok)
                        }
                    }
                }catch{
                    
                }
            }
        }else{
            self.displayAlert(title: "Error", message: "Enter account and password")
        }
        
        if userName.text == "admin"{
            self.displayAlert(title: "Error", message: "Can not use admin")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
