//
//  EditVC.swift
//  302cem
//
//  Created by A on 20/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit
import Firebase

class EditAndRemoveVC: VC,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var blogs = [Blog]()
    
    var searchController: UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.layer.cornerRadius = 10
        self.printLog(msg: "\(self.cu.name) \(self.cu.email) \(self.cu.password)" )
        self.getBlogs()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "blogCell2", for: indexPath) as! BlogCell2
        
        cell.postTitle.text = "Title: \(blogs[indexPath.row].title)"
        cell.postDate.text =  "Author: \(blogs[indexPath.row].name)"
        cell.postContent.text = "\(blogs[indexPath.row].content)"
        cell.postAuthor.text = "\(blogs[indexPath.row].date)"
        
        Storage.storage().reference(forURL: self.cf.fbs).child("\(self.blogs[indexPath.row].photo).jpg").getData(maxSize: Int64(self.cf.size), completion: { (data, error) in
            if error != nil {
                self.printLog(msg: error!.localizedDescription)
                return
            }
            if let d = data{
                cell.postImage.image = UIImage.init(data:d)
            }
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at:indexPath) as! BlogCell2
        
        if self.cu.name != "admin"{
            let title = self.blogs[indexPath.row].title
            let name = self.blogs[indexPath.row].name
            let content = self.blogs[indexPath.row].content
            let date = self.blogs[indexPath.row].date
            self.cu.photo = self.blogs[indexPath.row].photo
            self.cu.image = cell.postImage.image!
            self.cu.author = name
            self.cu.content = content
            self.cu.date = date
            self.cu.title = title
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                let _:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "EditBlogVC") as! EditBlogVC
                self.present(next, animated: true, completion: nil)
                
            }
        }
        
    }
    
    @IBAction func reload(_ sender: Any) {
        self.getBlogs()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
            var b = ""
            
            self.cu.title = self.blogs[indexPath.row].title
            self.cu.content = self.blogs[indexPath.row].content
            self.cu.date = self.blogs[indexPath.row].date
            self.cu.photo = self.blogs[indexPath.row].photo
            self.cu.author = self.blogs[indexPath.row].name
            
            if cu.name == "admin"{
                 b = "name=\(self.cu.author)&title=\(self.cu.title)&content=\(self.cu.content)&photo=\(self.cu.photo)&date=\(self.cu.date)&uuid=\(self.blogs[indexPath.row].uuid)"
            }else{
                b = "name=\(self.cu.author)&title=\(self.cu.title)&content=\(self.cu.content)&photo=\(self.cu.photo)&date=\(self.cu.date)&uuid=\(self.cu.uuid)"
            }
            
            self.postMothod(url: self.cf.removeBlog, body: b) { (d) in
                do{
                    self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                    
                    self.printLog(msg: "--------------------self.okStruct!.ok \(self.okStruct!.ok)")
                    
                    if self.okStruct!.ok == "successful" {
                        
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Done", message: "OK")
                            self.blogs.remove(at: indexPath.row)
                            self.tableView.reloadData()
                        }
                        
                    } else{
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Error", message: self.okStruct!.ok)
                        }
                    }
                }catch{
                    
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
}

extension EditAndRemoveVC {
    func getBlogs(){
                
        if self.cu.name == "admin"{
            if let url = URL(string: self.cf.blog){
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                let datatask = URLSession.shared.dataTask(with: request, completionHandler: {data, respond, err in
                    
                    if(data == nil){
                        print("nil data")
                        return
                    }
                    
                    do {
                        self.blogs = try JSONDecoder().decode([Blog].self, from: data!)
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    } catch {
                        print(error)
                    }
                })
                datatask.resume()
            }
        }else{
            let b = "uuid=\(self.cu.uuid)"
            self.postMothod(url: cf.myBlog, body: b) { (d) in
                do{
                    self.blogs = try JSONDecoder().decode([Blog].self, from: d)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                }
            }
        }
    }
}
