//
//  PostVC.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class PostVC: VC, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var postPhotoBtn: UIButton!
    @IBOutlet weak var postTitle: UITextField!
    @IBOutlet weak var postContent: UITextView!
    @IBOutlet weak var posrBtn: UIButton!
    @IBOutlet weak var postPhoto: UIImageView!
    @IBOutlet weak var v: UIView!
    
    let image = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postContent.backgroundColor = UIColor.white
        postContent.textColor = UIColor.black
        postContent.font = UIFont(name: "Helvetica-Light", size: 20)
        postContent.textAlignment = .left
        postContent.text = "123..."
        postContent.keyboardType = .default
        postContent.returnKeyType = .default
        
        self.postPhotoBtn.layer.cornerRadius = 10
        self.postTitle.layer.cornerRadius = 10
        self.postContent.layer.cornerRadius = 10
        self.posrBtn.layer.cornerRadius = 10
        self.postPhoto.layer.cornerRadius = 10
        self.v.layer.cornerRadius = 10
        
        printLog(msg: cu.name)
        
        if cu.name == "admin" {
            posrBtn.isUserInteractionEnabled = false
            postPhoto.isUserInteractionEnabled = false
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
    }
    
    @IBAction func submit(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: Date())
        printLog(msg: strDate)
        
        let uniqueString = NSUUID().uuidString
        
        if let data = self.postPhoto.image!.jpegData(compressionQuality: 0.1){
            
            let storageRef = Storage.storage().reference().child("\(uniqueString).jpg")
            let _ = storageRef.putData(data, metadata: nil) { (metadata, error) in
                if error == nil{
                    
                    let b = "name=\(self.cu.name)&title=\(self.postTitle.text!)&content=\(self.postContent.text!)&photo=\(uniqueString)&date=\(strDate)&uuid=\(self.cu.uuid)"
                    
                    self.postMothod(url: self.cf.addBlog, body: b) { (d) in
                        do{
                            self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                            
                            self.printLog(msg: "--------------------self.okStruct!.ok \(self.okStruct!.ok)")
                            
                            if self.okStruct!.ok == "successful" {
                                
                                DispatchQueue.main.async {
                                    self.displayAlert(title: "Done", message: "OK")
                                }
                                
                            } else{
                                DispatchQueue.main.async {
                                    self.displayAlert(title: "Error", message: self.okStruct!.ok)
                                }
                            }
                        }catch{
                            
                        }
                    }
                    
                }else{
                    self.printLog(msg: error!.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func savePhoto(_ sender: Any) {
        
        self.image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.image.sourceType = .camera
                self.present(self.image,animated:  true ,completion: nil)
            }else{
                print("Camera not available")
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            self.image.sourceType = .photoLibrary
            self.present(self.image,animated:  true ,completion: nil)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet,animated: true,completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func hideKeyboard(tapG:UITapGestureRecognizer){
        self.postContent.resignFirstResponder()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.postPhoto.image = image
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
}
extension PostVC{
    
}
