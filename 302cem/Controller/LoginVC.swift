//
//  LoginView.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class LoginVC: VC, UITextFieldDelegate {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var v: UIView!
    
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.sendSubviewToBack(v)
        self.v.layer.cornerRadius = 10
        self.userName.layer.cornerRadius = 10
        self.userPassword.layer.cornerRadius = 10
        self.login.layer.cornerRadius = 10
        self.register.layer.cornerRadius = 10
    }
    
    @IBAction func login(_ sender: Any){
        if (userName.text != "" && userPassword.text != "") {
            
            let b = "name=\(userName.text!)&password=\(userPassword.text!)"
            
            self.postMothod(url: cf.login, body: b) { (d) in
                do{
                    self.user = try JSONDecoder().decode(User.self, from: d)
                    
                    DispatchQueue.main.async {
                        if self.user!.email == self.userName.text! {
                            
                            self.cu.name = self.user!.name
                            self.cu.email = self.user!.email
                            self.cu.uuid = self.user!.uuid
                            self.cu.password = self.user!.password
                            
                            DispatchQueue.main.async {
                                let details = self.storyboard?.instantiateViewController(withIdentifier: "MyTBC") as! UITabBarController
                                details.modalPresentationStyle = .fullScreen
                                self.present(details, animated: true, completion: nil)
                            }
                        } else{
                            DispatchQueue.main.async {
                                self.displayAlert(title: "Error", message: "Incorrect password") }
                        }
                    }
                }catch{
                }
            }
        }else{
            let alert = UIAlertController(title: "error", message: "Enter account and password", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
    }
    
    @IBAction func cancel(segue: UIStoryboardSegue){
        if (segue.identifier == "cancel"){
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
