//
//  SettingVC.swift
//  302cem
//
//  Created by A on 21/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit

class SettingVC: VC,UITextFieldDelegate {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var nameBtn: UIButton!
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var v: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.v.layer.cornerRadius = 10
        self.nameTF.layer.cornerRadius = 10
        self.passwordTF.layer.cornerRadius = 10
        self.nameBtn.layer.cornerRadius = 10
        self.passwordBtn.layer.cornerRadius = 10
        self.logoutBtn.layer.cornerRadius = 10
        
        if cu.name == "admin" {
            passwordBtn.isUserInteractionEnabled = false
            nameBtn.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func changeName(_ sender: Any) {
        if self.nameTF.text != ""{
            let b = "name=\(self.nameTF.text!)&password=\(self.cu.password)&uuid=\(self.cu.uuid)&email=\(self.cu.email)&oname=\(self.cu.name)"
            
            self.postMothod(url: self.cf.changeUserInfo, body: b) { (d) in
                
                do{
                    self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                    
                    if self.okStruct!.ok == "successful" {
                        
                        DispatchQueue.main.async {
                            self.cu.name = self.nameTF.text!
                            self.displayAlert(title: "Done", message: "OK")
                        }
                    } else{
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Error", message: self.okStruct!.ok)
                        }
                    }
                }catch{
                    
                }
            }
        }else{
            
        }
        
        if nameTF.text == "admin"{
            self.displayAlert(title: "Error", message: "Can not use admin")
        }
    }
    
    
    @IBAction func changePassword(_ sender: Any) {
        if self.passwordTF.text != ""{
            let b = "name=\(self.cu.name)&password=\(self.passwordTF.text!)&uuid=\(self.cu.uuid)&email=\(self.cu.email)&opassword=\(self.cu.password)"
            self.postMothod(url: self.cf.changeUserPassword, body: b) { (d) in
                do{
                    self.okStruct = try JSONDecoder().decode(OkStruct.self, from: d)
                    
                    if self.okStruct!.ok == "successful" {
                        
                        DispatchQueue.main.async {
                            self.cu.password = self.passwordTF.text!
                            self.displayAlert(title: "Done", message: "OK")
                        }
                    } else{
                        DispatchQueue.main.async {
                            self.displayAlert(title: "Error", message: self.okStruct!.ok)
                        }
                    }
                }catch{
                }
            }
        }
    }
    
    @IBAction func Logout(_ sender: Any) {
        DispatchQueue.main.async {
            let details = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            details.modalPresentationStyle = .fullScreen
            self.present(details, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
