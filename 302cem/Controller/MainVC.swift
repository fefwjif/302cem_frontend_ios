//
//  MainVC.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import UIKit
import Firebase

class MainVC: VC, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var searchController: UISearchController!
    
    var blogs = [Blog]()
    var blogs2 = [Blog]()
    var blogs3: [Blog] = [Blog]()
    var isShowSearchResult: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchBar.placeholder = "search by title, content, date or author"
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        self.getblog()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isShowSearchResult {
            return self.blogs3.count
        } else {
            return self.blogs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "blogCell", for: indexPath) as! BlogCell
        
        if !self.isShowSearchResult {
            
            cell.blogTitle.text = "\(blogs[indexPath.row].date)"
            cell.blogDate.text = "Author: \(blogs[indexPath.row].name)"
            cell.blogContent.text = "\(blogs[indexPath.row].content)"
            cell.blogAuthor.text = "Title: \(blogs[indexPath.row].title)"
            
            Storage.storage().reference(forURL: self.cf.fbs).child("\(self.blogs[indexPath.row].photo).jpg").getData(maxSize: Int64(self.cf.size), completion: { (data, error) in
                if error != nil {
                    self.printLog(msg: error!.localizedDescription)
                    return
                }
                if let d = data{
                    cell.blogImage.image = UIImage.init(data:d)
                }
            })
            return cell
        }else {
            cell.blogTitle.text = "\(blogs3[indexPath.row].date)"
            cell.blogDate.text = "\(blogs3[indexPath.row].name)"
            cell.blogContent.text = "\(blogs3[indexPath.row].content)"
            cell.blogAuthor.text = "\(blogs3[indexPath.row].title)"
            
            Storage.storage().reference(forURL: self.cf.fbs).child("\(self.blogs3[indexPath.row].photo).jpg").getData(maxSize: Int64(self.cf.size), completion: { (data, error) in
                if error != nil {
                    self.printLog(msg: error!.localizedDescription)
                    return
                }
                if let d = data{
                    cell.blogImage.image = UIImage.init(data:d)
                }
            })
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 450
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if self.searchController.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count == 0 {
            return
        }
        self.filterDataSource()
    }
    
    @IBAction func addPost(_ sender: Any) {
        let _:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let next = self.storyboard?.instantiateViewController(withIdentifier: "PostVC") as! PostVC
        self.present(next, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.resignFirstResponder()
    }
    
    @IBAction func reLoad(_ sender: Any) {
        self.getblog()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isShowSearchResult = false
        self.tableView.reloadData()
    }
    
    func filterDataSource() {
        self.blogs3 = [Blog]()
        
        self.blogs3 = self.blogs2.filter { (b) -> Bool in
            return b.name.lowercased().range(of: self.searchController.searchBar.text!.lowercased()) != nil || b.title.lowercased().range(of: self.searchController.searchBar.text!.lowercased()) != nil || b.content.lowercased().range(of: self.searchController.searchBar.text!.lowercased()) != nil || (b.date.lowercased().range(of: self.searchController.searchBar.text!.lowercased()) != nil)
        }
        
        if self.blogs3.count > 0 {
            self.isShowSearchResult = true
            self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.init(rawValue: 1)!
        } else {
            self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none //
        }
        
        self.tableView.reloadData()
    }
}

extension MainVC {
    func getblog(){
        
        self.isShowSearchResult = false
        
        if let url = URL(string: self.cf.blog){
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let datatask = URLSession.shared.dataTask(with: request, completionHandler: {data, respond, err in
                
                if(data == nil){
                    print("nil data")
                    return
                }
                
                do {                 
                    self.blogs = try JSONDecoder().decode([Blog].self, from: data!)
                    self.blogs2 = self.blogs
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                } catch {
                    print(error)
                }
            })
            datatask.resume()
        }
    }
}
