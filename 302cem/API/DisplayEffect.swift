//
//  DisplayEffect.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import Foundation
import UIKit

protocol DisplayEffectProtocol {
    
func displayAlert(title:String, message:String)
    
}
