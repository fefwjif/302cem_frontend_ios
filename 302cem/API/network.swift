//
//  network.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import Foundation
import UIKit

protocol PostRequestProtocol {
    func postMothod(url:String, body :String, myCallBack:@escaping (_ data: Data)  -> ())
    
    func printLog(msg:Any)
}
