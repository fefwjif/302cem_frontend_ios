//
//  CurrentUser.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import Foundation
import UIKit

class CurrentUser: NSObject {
    
    var title = ""
    var content = ""
    var photo = ""
    var date = ""
    var author = ""
    
    var image = UIImage.init()
    
    var name = ""
    var email = ""
    var password = ""
    var uuid = ""
    
    static let sharedInstance = CurrentUser()
    
}
