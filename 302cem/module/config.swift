//
//  config.swift
//  302cem
//
//  Created by A on 19/12/2019.
//  Copyright © 2019 A. All rights reserved.
//

import Foundation

class config{
    
    let urlStr = "http://192.168.0.195:3000"
    var blog:String
    var register:String
    var admin:String
    var login:String
    var addBlog:String
    var removeBlog:String
    var editBlog:String
    var changeUserInfo:String
    var changeUserPassword:String
    var message:String
    var sendMessage:String
    var myBlog:String
    
    var fbs:String
    var size:Int64
    
    init() {
        self.blog = "\(urlStr)/blogs/"
        self.register = "\(urlStr)/register/"
        self.login = "\(urlStr)/login/"
        self.admin = "\(urlStr)/blogs/"
        self.addBlog = "\(urlStr)/blogs/add/"
        self.size = 1 * 1024 * 1024
        self.fbs = "gs://cem-74746.appspot.com/"
        self.removeBlog = "\(urlStr)/blogs/remove/"
        self.editBlog = "\(urlStr)/blogs/edit/"
        self.changeUserInfo = "\(urlStr)/users/editInfo/"
        self.changeUserPassword = "\(urlStr)/users/editPassword/"
        self.message = "\(urlStr)/message/"
        self.sendMessage = "\(urlStr)/message/send/"
        self.myBlog = "\(urlStr)/blogs/myBlogs/"
    }
    
}

struct DataBase {
    var name = "name"
    var password = "password"
}

struct Blog  : Codable{
    var name = ""
    var title = ""
    var content = ""
    var photo = ""
    var date = ""
    var uuid = ""
}

struct OkStruct : Codable{
    var ok:String
}

struct User  : Codable{
    var name = ""
    var email = ""
    var password = ""
    var uuid = ""
}

struct Message  : Codable{
    var recipient = ""
    var message = ""
    var uuid = ""
    var sender = ""
}
